
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Presto</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://bootswatch.com/5/flatly/bootstrap.css">
        <link rel="stylesheet" href="#">
       
    </head>
    <body>
        <!-- navbar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
        <div class="container">
                    <a class="navbar-brand" href="{{env('APP_URL')}}">Presto</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                    <a class="nav-link" href="{{env('APP_URL')}}/chi-siamo">Chi siamo</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{env('APP_URL')}}/servizi">Servizi</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="">Prodotti</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{env('APP_URL')}}/contatti">Contatti</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{env('APP_URL')}}/announcemet/crate">Crea annuncio</a>
                    </li>
                </ul>
                    <div class="dropdown ml-auto">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                            Area riservata
                        </button>
                        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                            <li><a class="dropdown-item active" href="{{env('APP_URL')}}/register">Registrati</a></li>
                            <li><a class="dropdown-item" href="{{env('APP_URL')}}/login">Login</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#">Separated link</a></li>
                        </ul>
                    </div>
                </ul>
            </div>
        </div>
    </nav> 
       <!-- Main container -->
<div class="container" id="main" style="margin: top 100px;">
    <div class="row">
    <div class="col-9">
           @yield('content')
        </div>
        <!-- card -->
        <div class="col-3">
            
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                     <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>

            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                     <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>  
             <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                     <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                     <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>

        </div>
    </div>
</div>
<footer class="bg-dark">
    <hr>
    <p class="container text-light">Copyright &copy; 2022</p>
</footer>
         
         
        
     <script src="https://bootswatch.com/_vendor/jquery/dist/jquery.min.js"></script>     
     <script src="https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>     
     <script src="https://bootswatch.com/_vendor/prismjs/prism.js"></script> 
     <script src="#"></script>  
    </body>
</html>



